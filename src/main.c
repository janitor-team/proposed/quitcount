/*
 * quitcount - Copyright (c) 2008-2009 Colin Leroy <colin@colino.net>
 * A little status icon to keep track of what you saved since you quit
 * smoking.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <time.h>

#define QUITCOUNT_RESPONSE_PREFERENCES 234

time_t stop_date = (time_t)0;
int num_per_day;
float price_per_pack = 5.0;
int pack_size = 20;
float unit_price = 0;
char *currency = NULL;
float nicotine_per_cig = 1.1;
float tar_per_cig = 14.0;

GtkWidget *stop_date_calendar;
GtkAdjustment *num_per_day_adj;
GtkWidget *num_per_day_spin;
GtkAdjustment *price_per_pack_adj;
GtkWidget *price_per_pack_spin;
GtkWidget *currency_entry;
GtkAdjustment *pack_size_adj;
GtkWidget *pack_size_spin;

GtkAdjustment *nicotine_per_cig_adj;
GtkWidget *nicotine_per_cig_spin;
GtkAdjustment *tar_per_cig_adj;
GtkWidget *tar_per_cig_spin;

GtkStatusIcon *icon = NULL;
static void set_icon(gboolean anniversary)
{
	gchar *path;
	static gboolean last_anniversary = FALSE;
	if (!anniversary)
		path = g_strconcat(ICONSDIR, G_DIR_SEPARATOR_S, PACKAGE, ".svg", NULL);
	else
		path = g_strconcat(ICONSDIR, G_DIR_SEPARATOR_S, PACKAGE, "-tu.svg", NULL);

	if (!icon)
		icon = gtk_status_icon_new_from_file(path);
	else if (anniversary != last_anniversary)
		gtk_status_icon_set_from_file(GTK_STATUS_ICON(icon), path);

	last_anniversary = anniversary;
	
	g_free(path);
}

gboolean load_config(void)
{
	gchar *path = g_strconcat(
		g_get_user_config_dir(),
		G_DIR_SEPARATOR_S, PACKAGE, NULL);
	gchar *file = g_strconcat(
		path,
		G_DIR_SEPARATOR_S, "config", NULL);
	gchar *data;
	gchar **vars, **cur_var;

	if (!g_file_test(path, G_FILE_TEST_IS_DIR)) {
		if (g_mkdir_with_parents(path, 0755) < 0) {
			g_free(path);
			g_free(file);
			return FALSE;
		}
	}
	g_free(path);
	
	if (g_file_get_contents(file, &data, NULL, NULL) == FALSE) {
		printf("can't read %s\n", file);
		g_free(file);
		return FALSE;
	}
	g_free(file);
	
	vars = g_strsplit_set(data, "\n=", -1);
	g_free(data);

	cur_var = vars;
	while (cur_var && *cur_var && *(cur_var+1)) {
		if (!strcmp(*cur_var, "stop_date"))
			stop_date = (time_t)atoi(*(++cur_var));
		else if (!strcmp(*cur_var, "num_per_day"))
			num_per_day = atoi(*(++cur_var));
		else if (!strcmp(*cur_var, "price_per_pack"))
			price_per_pack = g_ascii_strtod(*(++cur_var), NULL);
		else if (!strcmp(*cur_var, "nicotine_per_cig"))
			nicotine_per_cig = g_ascii_strtod(*(++cur_var), NULL);
		else if (!strcmp(*cur_var, "tar_per_cig"))
			tar_per_cig = g_ascii_strtod(*(++cur_var), NULL);
		else if (!strcmp(*cur_var, "pack_size"))
			pack_size = atoi(*(++cur_var));
		else if (!strcmp(*cur_var, "currency"))
			currency = g_strdup(*(++cur_var));

		cur_var++;
	}
	g_strfreev(vars);

	if (pack_size <= 0)
		pack_size = 1;
	unit_price = price_per_pack/pack_size;

	return (stop_date != (time_t)0);
}

int write_config(void)
{
	gchar *path = g_strconcat(
		g_get_user_config_dir(),
		G_DIR_SEPARATOR_S, PACKAGE, NULL);
	gchar *file = g_strconcat(
		path,
		G_DIR_SEPARATOR_S, "config", NULL);
	gchar *data;
	gchar price_s[256], nicotine_s[256], tar_s[256];
	
	if (!g_file_test(path, G_FILE_TEST_IS_DIR)) {
		if (g_mkdir_with_parents(path, 0755) < 0) {
			g_free(path);
			g_free(file);
			return FALSE;
		}
	}
	g_free(path);
	
	while(strstr(currency, "="))
		*(strstr(currency, "=")) = '-';
		
	g_ascii_dtostr(price_s, 255, price_per_pack);
	g_ascii_dtostr(nicotine_s, 255, nicotine_per_cig);
	g_ascii_dtostr(tar_s, 255, tar_per_cig);
	data = g_strdup_printf(
		"stop_date=%ld\n"
		"num_per_day=%d\n"
		"price_per_pack=%s\n"
		"nicotine_per_cig=%s\n"
		"tar_per_cig=%s\n"
		"pack_size=%d\n"
		"currency=%s\n",
			stop_date,
			num_per_day,
			price_s,
			nicotine_s,
			tar_s,
			pack_size,
			currency?currency:"€");
	
	if (g_file_set_contents(file, data, -1, NULL) == FALSE) {
		printf("can't write %s\n", file);
		g_free(file);
		g_free(data);
		return FALSE;
	}
	g_free(file);
	g_free(data);

	return TRUE;
}

static gboolean check_anniversary(gpointer data)
{
	time_t now = time(NULL);
	int num_day = (now - stop_date)/86400;
	int num_cig = num_day * num_per_day;
	float price, life;
	gboolean anniversary = FALSE;
	struct tm *tmtime;
	int now_day, quit_day, now_month, quit_month, now_year, quit_year;
	gchar *str = NULL;

	if (num_cig == 0)
		return TRUE;

	tmtime = gmtime(&stop_date);
	quit_day = tmtime->tm_mday;
	quit_month = tmtime->tm_mon;
	quit_year = tmtime->tm_year;
	tmtime = gmtime(&now);
	now_day = tmtime->tm_mday;
	now_month = tmtime->tm_mon;
	now_year = tmtime->tm_year;

	life = num_cig*10.0;
	price = unit_price*num_cig;
	
	if ((int)life < 7*60*24 && ((int)life) % (60*24) == 0) {
		str = g_strdup_printf(ngettext("Life expectancy gain: %d day!","Life expectancy gain: %d days!",(int)life/(60*24)), (int)life/(60*24));
		anniversary = TRUE;
	} else if ((int)life < 30*60*24 && ((int)life) % (7*60*24) == 0) {
		str = g_strdup_printf(ngettext("Life expectancy gain: %d week!","Life expectancy gain: %d weeks!",(int)life/(60*24*7)), (int)life/(60*24*7));
		anniversary = TRUE;
	} else if ((int)life < 365*60*24 && ((int)life) % (30*60*24) == 0) {
		str = g_strdup_printf(ngettext("Life expectancy gain: %d month!","Life expectancy gain: %d months!",(int)life/(60*24*30)), (int)life/(60*24*30));
		anniversary = TRUE;
	} else if ((int)life >= 365*60*24 && ((int)life) % (365*60*24) == 0) {
		str = g_strdup_printf(ngettext("Life expectancy gain: %d year!","Life expectancy gain: %d years!",(int)life/(365*60*24)), (int)life/(365*60*24));
		anniversary = TRUE;
	} 

	if ((int)price < 100 && ((int)price) % 10 == 0) {
		str = g_strdup_printf(_("%d %s saved!"),(int)price, currency);
		anniversary = TRUE;
	} else if ((int)price < 1000 && ((int)price) % 100 == 0) {
		str = g_strdup_printf(_("%d %s saved!"),(int)price, currency);
		anniversary = TRUE;
	} else if ((int)price < 10000 && ((int)price) % 1000 == 0) {
		str = g_strdup_printf(_("%d %s saved!"),(int)price, currency);
		anniversary = TRUE;
	} else if ((int)price < 100000 && ((int)price) % 10000 == 0) {
		str = g_strdup_printf(_("%d %s saved!"),(int)price, currency);
		anniversary = TRUE;
	}
	if (num_day < 31 && (num_day % 7) == 0) {
		str = g_strdup_printf(ngettext("You quit since %d week!", "You quit since %d weeks!",(int)num_day/7), (int)num_day/7);
		anniversary = TRUE;
	} else if (num_day < 363 && num_day > 1 && now_day == quit_day) {
		int nmon = 0;
		if (quit_month > now_month)
			now_month += 12;
		nmon = now_month-quit_month;
		str = g_strdup_printf(ngettext("You quit since %d month!", "You quit since %d months!", nmon), nmon);
		anniversary = TRUE;
	} else if (num_day >= 363 && now_day == quit_day && now_month == quit_month) {
		int nyear = 0;
		nyear = now_year-quit_year;
		str = g_strdup_printf(ngettext("You quit since %d year!", "You quit since %d years!",nyear), nyear);
		anniversary = TRUE;
	} else if (num_day <=500 && (num_day % 100) == 0) {
		str = g_strdup_printf(_("You quit since %d days!"), num_day);
		anniversary = TRUE;
	} else if (num_day >= 1000 && (num_day % 1000) == 0) {
		str = g_strdup_printf(_("You quit since %d days!"), num_day);
		anniversary = TRUE;
	}
	if (str)
		gtk_status_icon_set_tooltip_text(icon, str);
	else
		gtk_status_icon_set_has_tooltip(icon, FALSE);
	g_free(str);

	set_icon(anniversary);

	return TRUE;
}

static guint timeout_id = 0;
static void install_anniversary_watch(void)
{
	if (timeout_id > 0) {
		g_source_remove(timeout_id);
		timeout_id = 0;
	}
	
	check_anniversary(NULL);
	timeout_id = g_timeout_add_seconds(3600, check_anniversary, NULL);
}

static GtkWidget *wrap(GtkWidget *inner, gchar *label)
{
	GtkWidget *grid = gtk_grid_new();
	GtkWidget *i;
	GtkWidget *title;
	GdkPixbuf *pixbuf;
	gchar *path = g_strconcat(ICONSDIR, G_DIR_SEPARATOR_S, PACKAGE, ".svg", NULL);

	pixbuf = gdk_pixbuf_new_from_file_at_size(path, 48, 48, NULL);
	i = gtk_image_new_from_pixbuf(pixbuf);
	g_object_unref(G_OBJECT(pixbuf));
	g_free(path);	
	title = gtk_label_new(label);
	gtk_label_set_use_markup(GTK_LABEL(title), TRUE);
	gtk_misc_set_alignment(GTK_MISC(title), 0, 0.5);
	gtk_grid_set_row_spacing(GTK_GRID(grid), 10);
	gtk_grid_set_row_spacing(GTK_GRID(grid), 10);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 10);
	gtk_grid_attach(GTK_GRID(grid), i, 0, 0, 1, 1);
	gtk_grid_attach(GTK_GRID(grid), title, 1, 0, 1, 1);
	gtk_grid_attach(GTK_GRID(grid), gtk_separator_new(GTK_ORIENTATION_HORIZONTAL), 0, 1 ,2, 1);
	gtk_grid_attach(GTK_GRID(grid), inner, 0, 2, 2, 1);
	gtk_widget_set_hexpand (grid, TRUE);
	gtk_widget_set_hexpand (title, TRUE);
	gtk_widget_set_halign (inner, GTK_ALIGN_CENTER);
	gtk_widget_set_margin_left(i, 10);
	gtk_widget_show_all(grid);
	return grid;
}

void show_config(gpointer object, gpointer user_data)
{
	static GtkWidget *dialog = NULL;
	GtkWidget *box;
	GtkWidget *grid;
	GtkWidget *label;
	int row = 0;
	GtkWidget *hbox;
	time_t now;
	struct tm *ctime;
	guint d, m, y;

	if (stop_date != 0)
		now = stop_date;
	else 
		now = time(NULL);

	ctime = gmtime(&now);
	d = ctime->tm_mday;
	m = ctime->tm_mon;
	y = ctime->tm_year;

	if (dialog != NULL)
		return;

	dialog = gtk_dialog_new_with_buttons(
			_("QuitCount configuration"),
			NULL, GTK_DIALOG_MODAL,
			GTK_STOCK_CLOSE, GTK_RESPONSE_ACCEPT,
			NULL);
			
	box = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
	
	grid = gtk_grid_new();
	
	gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 3);
	
	label = gtk_label_new(_("I quit on"));
	gtk_misc_set_alignment(GTK_MISC(label), 1, 0);
	gtk_grid_attach(GTK_GRID(grid), label, 0, row, 1, 1);
	
	stop_date_calendar = gtk_calendar_new();
	gtk_calendar_select_month(GTK_CALENDAR(stop_date_calendar),
					m,
					y + 1900);
	gtk_calendar_select_day(GTK_CALENDAR(stop_date_calendar),
					d);
	gtk_grid_attach(GTK_GRID(grid), stop_date_calendar, 1, row, 1, 1);
	
	row++;
	
	label = gtk_label_new(_("I smoked"));
	gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 0, row, 1, 1);
	
	num_per_day_adj = GTK_ADJUSTMENT(
		gtk_adjustment_new(num_per_day, 1.0, 100.0, 1.0, 1.0, 0.0));
	num_per_day_spin = gtk_spin_button_new(num_per_day_adj, 1.0, 0);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(num_per_day_spin), TRUE);
	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
	label = gtk_label_new(_("cigarettes per day"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	
	gtk_box_pack_start(GTK_BOX(hbox), num_per_day_spin, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
	gtk_grid_attach(GTK_GRID(grid), hbox, 1, row, 1, 1);
	
	row++;
	
	label = gtk_label_new(_("Each contained"));
	gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 0, row, 1, 1);
	
	nicotine_per_cig_adj = GTK_ADJUSTMENT(
		gtk_adjustment_new(nicotine_per_cig, 0.1, 3.0, 0.1, 0.1, 0.0));
	nicotine_per_cig_spin = gtk_spin_button_new(nicotine_per_cig_adj, 0.1, 1);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(nicotine_per_cig_spin), TRUE);
	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
	label = gtk_label_new(_("mg of nicotine"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	
	gtk_box_pack_start(GTK_BOX(hbox), nicotine_per_cig_spin, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
	gtk_grid_attach(GTK_GRID(grid), hbox, 1, row, 1, 1);
	
	row++;
	
	tar_per_cig_adj = GTK_ADJUSTMENT(
		gtk_adjustment_new(tar_per_cig, 1.0, 20.0, 0.5, 0.5, 0.0));
	tar_per_cig_spin = gtk_spin_button_new(tar_per_cig_adj, 0.5, 1);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(tar_per_cig_spin), TRUE);
	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
	label = gtk_label_new(_("mg of tar"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	
	gtk_box_pack_start(GTK_BOX(hbox), tar_per_cig_spin, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
	gtk_grid_attach(GTK_GRID(grid), hbox, 1, row, 1, 1);
	
	row++;
	
	label = gtk_label_new(_("It cost"));
	gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 0, row, 1, 1);
	
	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
	price_per_pack_adj = GTK_ADJUSTMENT(
		gtk_adjustment_new(price_per_pack, 0.1, 1000.0, 0.1, 0.1, 0.0));
	price_per_pack_spin = gtk_spin_button_new(price_per_pack_adj, 0.1, 2);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(price_per_pack_spin), TRUE);
	gtk_box_pack_start(GTK_BOX(hbox), price_per_pack_spin, FALSE, FALSE, 0);
	
	currency_entry = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(currency_entry), currency?currency:"€");
	gtk_entry_set_width_chars(GTK_ENTRY(currency_entry), 3);
	gtk_box_pack_start(GTK_BOX(hbox), currency_entry, FALSE, FALSE, 0);

	label = gtk_label_new(_("per pack of"));
	gtk_misc_set_alignment(GTK_MISC(label), 0.5, 0.5);
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

	pack_size_adj = GTK_ADJUSTMENT(
		gtk_adjustment_new(pack_size, 1.0, 100.0, 1.0, 1.0, 0.0));
	pack_size_spin = gtk_spin_button_new(pack_size_adj, 1.0, 0);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(pack_size_spin), TRUE);
	
	gtk_box_pack_start(GTK_BOX(hbox), pack_size_spin, TRUE, TRUE, 0);
	gtk_grid_attach(GTK_GRID(grid), hbox, 1, row, 1, 1);
	
	gtk_widget_show_all(grid);
	gtk_box_pack_start(GTK_BOX(box), wrap(grid, _("<span size='x-large'><b>QuitCount - configuration</b></span>")), 
			FALSE, FALSE, 10);
	gtk_widget_set_size_request(dialog, 500, -1);
	gtk_dialog_run(GTK_DIALOG(dialog));
	
	gtk_calendar_get_date(GTK_CALENDAR(stop_date_calendar),
				&y, &m, &d);
	ctime->tm_year = y - 1900;
	ctime->tm_mon  = m;
	ctime->tm_mday = d;
	
	ctime->tm_hour = ctime->tm_min = ctime->tm_sec = 1;
	
	stop_date = mktime(ctime);
	num_per_day = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(num_per_day_spin));
	price_per_pack = gtk_spin_button_get_value(GTK_SPIN_BUTTON(price_per_pack_spin));
	pack_size = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(pack_size_spin));
	unit_price = price_per_pack/pack_size;
	nicotine_per_cig = gtk_spin_button_get_value(GTK_SPIN_BUTTON(nicotine_per_cig_spin));
	tar_per_cig = gtk_spin_button_get_value(GTK_SPIN_BUTTON(tar_per_cig_spin));
	g_free(currency);
	currency = gtk_editable_get_chars(GTK_EDITABLE(currency_entry), 0, -1);
	write_config();
	install_anniversary_watch();

	gtk_widget_destroy(dialog);
	dialog = NULL;
}

static void do_quit(gpointer object, gpointer user_data)
{
	GtkApplication *app = GTK_APPLICATION(user_data);
	GList *windows = gtk_application_get_windows(app);
	if (windows)
		gtk_application_remove_window(app, GTK_WINDOW(windows->data));
}

static void do_about(gpointer object, gpointer user_data)
{
	GtkWidget *dialog = gtk_about_dialog_new();
	
	const gchar *artists[] = {"Logo: PMOX on commons.wikimedia.org", 
				  "Thumb up: Pratheepps on commons.wikimedia.org",
				  NULL};
	const gchar *authors[] = {"Colin Leroy <colin@colino.net>", NULL};
	
	gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "QuitCount");
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), VERSION);
	if (icon != NULL)
		gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), gtk_status_icon_get_pixbuf(icon));
	gtk_about_dialog_set_artists(GTK_ABOUT_DIALOG(dialog), (const gchar **)artists);
	gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(dialog), (const gchar **)authors);
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), 
		_("Copyright (C) 2009-2013, Colin Leroy <colin@colino.net>\nGPL version 3 or later"));
	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog), 
		"http://quitcount.sf.net/");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

static void do_activate(gpointer object, gpointer user_data)
{
	time_t now = time(NULL);
	gchar *buf;
	int num_day = (now - stop_date)/86400;
	int num_cig = num_day * num_per_day;
	float nicotine, tar, life;
	const char *n_unit, *t_unit, *l_unit;
	gint response;
	GtkWidget *grid, *label;
	gint row = 0;
	GtkWidget *box;
	static GtkWidget *dialog = NULL;

	if (dialog != NULL) {
		gtk_dialog_response(GTK_DIALOG(dialog), GTK_RESPONSE_ACCEPT);
		return;
	}

	dialog = gtk_dialog_new_with_buttons(
			_("QuitCount statistics"),
			NULL, GTK_DIALOG_MODAL,
			GTK_STOCK_PREFERENCES, QUITCOUNT_RESPONSE_PREFERENCES,
			GTK_STOCK_CLOSE, GTK_RESPONSE_ACCEPT,
			NULL);

	box = gtk_dialog_get_content_area(GTK_DIALOG(dialog));

	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_ACCEPT);

	grid = gtk_grid_new();
	
	gtk_grid_set_row_spacing(GTK_GRID(grid), 2);
	gtk_grid_set_column_spacing(GTK_GRID(grid), 5);
	
	buf = g_strdup_printf("<span size='large'><b>%d</b></span>", num_day);
	label = gtk_label_new(buf);
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	g_free(buf);
	gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 0, row, 1, 1);
	
	label = gtk_label_new(_("<span size='large'>days since I quit</span>"));
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 1, row, 1, 1);
	
	row++;
	
	if (num_day > 7) {
		int num_year, num_mon, num_week;
		gchar *fmt = NULL, *yfmt = NULL, *mfmt = NULL, *wfmt = NULL;
		num_year = num_day/365;
		num_day = (num_day%365);
		num_mon = num_day/30;
		num_day = (num_day%30);
		num_week = num_day/7;
		num_day = (num_day%7);

		if (num_year > 0)
			yfmt = g_strdup_printf(ngettext("%d year", "%d years", num_year), num_year);
		if (num_mon > 0)
			mfmt = g_strdup_printf(ngettext("%d month", "%d months", num_mon), num_mon);
		if (num_week > 0)
			wfmt = g_strdup_printf(ngettext("%d week", "%d weeks", num_week), num_week);

		fmt = g_strdup_printf("%s%s%s%s%s",
			yfmt ? yfmt:"",
			yfmt && mfmt ? ", ":"",
			mfmt ? mfmt:"",
			(yfmt||mfmt) && wfmt ? ", ":"",
			wfmt ? wfmt:""
			);
			
		label = gtk_label_new(fmt);
		gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
		g_free(fmt);
		g_free(yfmt);
		g_free(mfmt);
		g_free(wfmt);
		gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
		gtk_grid_attach(GTK_GRID(grid), label, 1, row, 1, 1);

		row++;

	}
	
	
	buf = g_strdup_printf("<span size='large'><b>%d</b></span>", num_cig);
	label = gtk_label_new(buf);
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	g_free(buf);
	gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 0, row, 1, 1);
	
	label = gtk_label_new(_("<span size='large'>cigarettes not smoked</span>"));
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 1, row, 1, 1);
	
	row++;
	
	nicotine = nicotine_per_cig*num_cig;
	tar = tar_per_cig*num_cig;

	if (nicotine > 1000000) {
		n_unit = _("Kg");
		nicotine /= 1000000;
	} else if (nicotine > 1000) {
		n_unit = _("g");
		nicotine /= 1000;
	} else 
		n_unit = _("mg");

	if (tar > 1000000) {
		t_unit = _("Kg");
		tar /= 1000000;
	} else if (tar > 1000) {
		t_unit = _("g");
		tar /= 1000;
	} else 
		t_unit = _("mg");

	buf = g_strdup_printf(_("Nicotine: %.2f %s\nTar: %.2f %s"),
		nicotine, n_unit, tar, t_unit);
	label = gtk_label_new(buf);
	g_free(buf);
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 1, row, 1, 1);
	
	row++;
	
	buf = g_strdup_printf("<span size='large'><b>%.2f</b></span>", num_cig * unit_price);
	label = gtk_label_new(buf);
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	g_free(buf);
	gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 0, row, 1, 1);
	
	buf = g_strdup_printf(_("<span size='large'>%s saved</span>"), currency);
	label = gtk_label_new(buf);
	g_free(buf);
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 1, row, 1, 1);
	
	row++;
	
	life = num_cig*10.0;
	if (life > 60*24*365) {
		life /= (60*24*365);
		l_unit = ngettext("year","years",(int)life);
	} else if (life > 60*24*30) {
		life /= (60*24*30);
		l_unit = ngettext("month","months",(int)life);
	} else if (life > 60*24*7) {
		life /= (60*24*7);
		l_unit = ngettext("week","weeks",(int)life);
	} else if (life > 60*24) {
		life /= (60*24);
		l_unit = ngettext("day","days",(int)life);
	} else if (life > 60) {
		life /= (60);
		l_unit = ngettext("hour","hours",(int)life);
	} else
		l_unit = ngettext("minute","minutes",(int)life);

	buf = g_strdup_printf("<span size='large'><b>%.2f %s</b></span>", life, l_unit);
	label = gtk_label_new(buf);
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	g_free(buf);
	gtk_misc_set_alignment(GTK_MISC(label), 1, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 0, row, 1, 1);
	
	label = gtk_label_new(_("<span size='large'>of life expectancy gained</span>"));
	gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_grid_attach(GTK_GRID(grid), label, 1, row, 1, 1);
	
	row++;
	
	gtk_widget_show_all(grid);
	gtk_box_pack_start(GTK_BOX(box), wrap(grid, 
		_("<span size='x-large'><b>QuitCount - statistics</b></span>")), FALSE, FALSE, 10);
	gtk_widget_set_size_request(dialog, 500, -1);
	response = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	dialog = NULL;

	if (response == QUITCOUNT_RESPONSE_PREFERENCES)
		show_config(NULL, NULL);
}

static void do_popup_menu(GtkStatusIcon *i, guint button, guint activate_time, 
			  gpointer user_data)
{
	GtkWidget *menu = gtk_menu_new();
	GtkWidget *item;
	GtkWidget *img;
	gchar *path = g_strconcat(ICONSDIR, G_DIR_SEPARATOR_S, PACKAGE, ".svg", NULL);
	gint w,h;
	GdkPixbuf *pixbuf;
	GtkApplication *app = GTK_APPLICATION(user_data);
	
	item = gtk_image_menu_item_new_from_stock(GTK_STOCK_PREFERENCES, NULL);

	gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
	gtk_widget_show(item);
	g_signal_connect(G_OBJECT(item), "activate", 
			 G_CALLBACK(show_config), NULL);

	if (!gtk_icon_size_lookup(GTK_ICON_SIZE_MENU, &w, &h)) {
		w = 16;
		h = 16;
	}

	pixbuf = gdk_pixbuf_new_from_file_at_size(path, w, h, NULL);
	g_free(path);	
	img = gtk_image_new_from_pixbuf(pixbuf);
	g_object_unref(G_OBJECT(pixbuf));

	item = gtk_image_menu_item_new_with_label(_("Statistics"));
	gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item), img);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
	gtk_widget_show(item);
	g_signal_connect(G_OBJECT(item), "activate", 
			 G_CALLBACK(do_activate), NULL);

	item = gtk_separator_menu_item_new();
	gtk_widget_show(item);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);

	item = gtk_image_menu_item_new_from_stock(GTK_STOCK_ABOUT, NULL);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
	gtk_widget_show(item);
	g_signal_connect(G_OBJECT(item), "activate", 
			 G_CALLBACK(do_about), NULL);
	item = gtk_image_menu_item_new_from_stock(GTK_STOCK_QUIT, NULL);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
	gtk_widget_show(item);
	g_signal_connect(G_OBJECT(item), "activate", 
			 G_CALLBACK(do_quit), app);
	gtk_menu_popup(GTK_MENU(menu), NULL, NULL, gtk_status_icon_position_menu, i,
			button, activate_time);
}

void run_quitcount(GtkApplication *app)
{
	GList *windows = gtk_application_get_windows(app);
	if (windows == NULL) {
		GtkWidget *dummy = gtk_window_new(GTK_WINDOW_TOPLEVEL);

		gtk_window_set_application(GTK_WINDOW(dummy), app);

		set_icon(FALSE);

		if (!load_config()) {
			currency = g_strdup("€");
			show_config(NULL, NULL);
		}
		install_anniversary_watch();
	
		g_signal_connect(G_OBJECT(icon), "popup-menu", 
				 G_CALLBACK(do_popup_menu), app);
		g_signal_connect(G_OBJECT(icon), "activate", 
				 G_CALLBACK(do_activate), app);
	} else if (stop_date != (time_t)0) {
		do_activate(NULL, NULL);
	}
}

int main(int argc, char *argv[])
{
	GtkApplication *app = NULL;
	int status;

	gtk_init(&argc, &argv);
	
	bindtextdomain(PACKAGE, LOCALEDIR);
	bind_textdomain_codeset (PACKAGE, "UTF-8");
	textdomain(PACKAGE);

	app = gtk_application_new("net.sf.quitcount", 0);
	g_signal_connect(G_OBJECT(app), "activate",
			 G_CALLBACK(run_quitcount), NULL);

	status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);

	return status;
}
